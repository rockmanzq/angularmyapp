import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InteractionService {

  private teacherMessageSource = new Subject<string>();
  teacherMessage$ = this.teacherMessageSource.asObservable(); // this is an observable

  constructor() { }

  sendMessage(message: string) {
    this.teacherMessageSource.next(message);
  }
}
