import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {IEmployee} from './employee';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  // tslint:disable-next-line:variable-name
  private _url = '/assets/data/employees.json';
  constructor(private http: HttpClient) { }
  // error handler from here: https://scotch.io/bar-talk/error-handling-with-angular-6-tips-and-best-practices192
  // https://www.tektutorialshub.com/angular/angular-http-error-handling/
  getEmployees(): Observable<IEmployee[]> {
    return this.http.get<IEmployee[]>(this._url)
      .pipe(
        retry(1),
        catchError(this.errorHandler)
      );
    // return [
    //   {id : 1, name : 'Andrew', age: 30},
    //   {id : 2, name : 'Tom', age: 30},
    //   {id : 3, name : 'Tim', age: 30},
    //   {id : 4, name : 'Sam', age: 30},
    // ];
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.message || 'Server error');
  }
}
