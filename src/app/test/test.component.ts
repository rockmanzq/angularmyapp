import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {InteractionService} from '../interaction.service';

// @Component({
//   selector: 'app-test',
//   templateUrl: './test.component.html',
//   styleUrls: ['./test.component.css']
// })

// inline template and style
@Component({
  selector: 'app-test',
  template: `
    {{loggedMessage}}
    <div>
        {{message}}
    </div>
    <h2>
        Welcome {{name}}
    </h2>
    <h2>
        {{"Welcome" + name}}
    </h2>
    <h2>
        {{name.toUpperCase()}}
    </h2>
    <h2>
        {{greetUser()}}
    </h2>
    <h2>
        {{siteUrl}}
    </h2>

<!--    // property binding-->
    <input [id]="myId" type="text" value="Vishwas">
    <input [disabled]="isDisabled" id={{myId}} type="text" value="Vishwas">
    <input bind-disabled="isDisabled" id={{myId}} type="text" value="Vishwas">

<!--    // class binding, should only use one way-->
    <h2 class="text-success">Codevolution</h2>
    <h2 [class]="successClass">Codevolution</h2>
<!--    <h2 class="text-special" [class]="successClass">Codevolution</h2>-->
    <h2 [class.text-danger]="hasError">Codevolution</h2>
    <h2 [ngClass]="messageClasses">Message</h2>

<!--    // style binding-->
    <h2 [style.color]="'orange'">Style Binding</h2>
    <h2 [style.color]="hasError ? 'red' : 'green'">Style Binding 2</h2>
    <h2 [style.color]="highlightColor">Style Binding 3</h2>
    <h2 [ngStyle]="titleStyles">Style Binding 4</h2>

<!--    // event binding, call onClick function!-->
    <button (click)="onClick($event)">Greet</button>
    <button (click)="greeting = 'Welcome Vishwas'">Greet Vishwas</button>
    <h2>{{greeting}}</h2>

<!--    //Template Reference Variables-->
    <input #myInput type="text">
    <button (click)="logMessage(myInput)"> Log </button>

<!--    two-way binding-->
    <input [(ngModel)]="name" type="text">
    {{name}}

<!--    split two-way binding-->
    <div>
        <input #nameRef type="text" [ngModel]="userName" (ngModelChange)="userName=$event">
        <p>Welcome {{ userName }}</p>
    </div>

    <div>
        <input type="text" [ngModel]="userName" (ngModelChange)="splitTwoWayBinding($event)">
        <p>Welcome {{ userName }}</p>
    </div>

<!--    Getters and Setters, bind getter and setter instead of property-->
    <div>
        <input type="text" [(ngModel)]="customerName">
        <p>Welcome {{ customerName }}</p>
    </div>

<!--    Structure Directives-->
    <h2 *ngIf="displayName; else nameElseBlock">
        Codevolution
    </h2>
    <ng-template #nameElseBlock>
        <h2>
            Name is hidden
        </h2>
    </ng-template>

    <div *ngIf="displayName; then thenBlock; else elseBlock"></div>
    <ng-template #thenBlock>
        <h2>Codevolution</h2>
    </ng-template>
    <ng-template #elseBlock>
        <h2>Hidden</h2>
    </ng-template>

    <div [ngSwitch]="color">
        <div *ngSwitchCase="'red'">You picked red color</div>
        <div *ngSwitchCase="'blue'">You picked blue color</div>
        <div *ngSwitchCase="'green'">You picked green color</div>
        <div *ngSwitchDefault>Pick again</div>
    </div>

<!--    can use odd, even first last-->
    <div *ngFor="let color of colors; index as i">
        <h2>{{i}} {{color}}</h2>
    </div>

    <div *ngFor="let color of colors; odd as o">
        <h2>{{o}} {{color}}</h2>
    </div>

<!--    Component Interaction-->
    <h2>
        {{"Hello " + parentName}}
    </h2>

    <button (click)=fireEvent()>Send Event</button>

<!--    pipes-->
    <h2>{{"Codevolution" | lowercase}}</h2>
    <h2>{{"Codevolution" | uppercase}}</h2>
    <h2>{{"Welcome to codevolution" | titlecase}}</h2>
    <h2>{{"Codevolution" | slice:3:6}}</h2>
    <h2>{{person | json}}</h2>
    <h2>{{5.678 | number:'1.2-3'}}</h2>
    <h2>{{5.678 | number:'3.4-5'}}</h2>
    <h2>{{5.678 | number:'3.1-2'}}</h2>

    <h2>{{0.25 | percent}}</h2>
    <h2>{{0.25 | currency}}</h2>
    <h2>{{0.25 | currency:'GBP'}}</h2>
    <h2>{{0.25 | currency:'GBP':'code'}}</h2>

    <h2>{{date}}</h2>
    <h2>{{date | date:'short'}}</h2>
    <h2>{{date | date:'shortDate'}}</h2>
    <h2>{{date | date:'shortTime'}}</h2>

  `,
  styles: [`
    div{
        color: red;
    }
    .text-success{
        color: green;
    }
    .text-danger{
        color: red;
    }
    .text-special{
        font-style: italic;
    }
    `]
})

export class TestComponent implements AfterViewInit, OnInit, OnChanges {
  public name = 'Interpolation';
  public siteUrl = window.location.href;
  public myId = 'testId';
  public isDisabled = true;
  userName: string;
  // tslint:disable-next-line:variable-name
  private _customerName: string;
  @ViewChild('nameRef') nameElementRef: ElementRef;
  public TRV = 'TRV';
  public message = 'message from child component';

  public successClass = 'text-success';
  public hasError = false;
  public isSpecial = true;
  public messageClasses = {
    'text-success': !this.hasError,
    'text-danger': this.hasError,
    'text-special': this.isSpecial
  };

  public highlightColor = 'orange';
  public titleStyles = {
    color: 'blue',
    fontStyle: 'italic',
  };

  public greeting = '';

  public displayName = false;
  public color = 'red';
  public colors = [ 'red', 'blue', 'green', 'yellow' ];

  // tslint:disable-next-line:no-input-rename
  @Input('parentData') public parentName;
  @Output() public childEvent = new EventEmitter<string>();
  // tslint:disable-next-line:variable-name
  private _loggedIn: boolean;
  loggedMessage: string;

  @Input() loggedInOnChange: boolean;

  public person = {
    firstName: 'John',
    lastName: 'Doe'
  };

  public date = new Date();

  constructor(private interactionService: InteractionService) { }

  ngOnInit(): void {
    this.interactionService.teacherMessage$
      .subscribe(
        (message) => {
          if (message === 'Good Morning') {
            alert('Good Morning teacher');
          } else if (message === 'Well Done') {
            alert('Thank you teacher');
          }
        }
      );
  }

  ngAfterViewInit() {
    this.nameElementRef.nativeElement.focus();
    console.log(this.nameElementRef);
  }

  greetUser() {
    return 'Hello ' + this.name;
  }

  onClick(event) {
    console.log(event);
    this.greeting = event.type;
  }

  logMessage(input) {
    console.log(input.value);
  }

  fireEvent() {
    this.childEvent.emit('Hey Codevolution');
  }

  splitTwoWayBinding(updatedValue) {
    this.userName = updatedValue;
    if (updatedValue === 'Vishwas') {
      alert('Welcome back Vishwas');
    }
  }

  get customerName(): string {
    return this._customerName;
  }

  set customerName(value: string) {
    this._customerName = value;
    if (value === 'Vishwas') {
      alert('Hello Vishwas');
    }
  }

  get loggedIn(): boolean {
    return this._loggedIn;
  }

  /**
   * this decorator indicates that the setter is an input property
   * The setter makes it possible to intercept the value change and execute
   * additional lines of code
   * @param value
   */
  @Input()
  set loggedIn(value: boolean) {
    this._loggedIn = value;
    if (value === true) {
      this.loggedMessage = 'Welcome back Vishwas';
    } else {
      this.loggedMessage = 'Please log in';
    }
  }

  /**
   * ngOnchange:
   * - !! ngOnChange will be triggered if any input has changed !!
   * - if there are multiple input properties, this is better
   * - and we could use previousValue and firstChange to execute additional logic.
   *
   * if there is only one input, above getters and setters are okay
   */
  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes); // changes contain loggedInOnChange and loggedIn
    const key = 'loggedInOnChange'; // just used to avoid “no-string-literal” rule
    const loggedInValue = changes[key];
    if (loggedInValue.currentValue === true) {
      this.loggedMessage = 'Welcome back Vishwas';
    } else {
      this.loggedMessage = 'Please log in';
    }
  }
}
