import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';

@Component({
  selector: 'app-department-detail',
  template: `
    <h3>You selected department with id {{departmentId}}</h3>

    <p>
        <button (click)="showOverview()">Overview</button>
        <button (click)="showContact()">Contact</button>
    </p>
    <router-outlet></router-outlet>

    <button (click)="goPrevious()">Previous</button>
    <button (click)="goNext()">Next</button>
    <div>
        <button (click)="gotoDepartments()">Back</button>
    </div>
  `,
  styles: []
})
export class DepartmentDetailComponent implements OnInit {
  public departmentId;
  constructor(private route: ActivatedRoute, private router: Router) { }

  /**
   * the drawback of using snapshot: goPrevious, id or url change
   * but view doesn't change, but refresh page will change view
   * the reason is that ngOnInit will not be called when routed back or next
   *
   * 为啥department-list那个nav可以work，因为那里call了ngOnInit
   *
   * 改成subscribe就不会有这个问题，this.departmentId始终可以得到更新
   */
  ngOnInit(): void {
    // tslint:disable-next-line:radix
    // const id = parseInt(this.route.snapshot.paramMap.get('id'));
    // this.departmentId = id;
    this.route.paramMap.subscribe((params: ParamMap) => {
      // tslint:disable-next-line:radix
      const id = parseInt(params.get('id'));
      this.departmentId = id;
    });
  }

  goPrevious() {
    const previousId = this.departmentId - 1;
    // this.router.navigate(['/departments', previousId]);
    this.router.navigate(['../', previousId], {relativeTo: this.route});
  }

  goNext() {
    const nextId = this.departmentId + 1;
    // this.router.navigate(['/departments', nextId]);
    this.router.navigate(['../', nextId], {relativeTo: this.route});
  }

  gotoDepartments() {
    /**
     * do a null check
     * optional route parameters, will show in below url after you click back
     * http://localhost:4200/departments;id=2
     * no need to config the optional route parameters in app-routing.moudle
     * like {path: 'departments/:id', component: DepartmentDetailComponent},
     */

    const selectedId = this.departmentId ? this.departmentId : null;
    // compare with: this.router.navigate(['/departments', department.id]);
    // this.router.navigate(['/departments', {id: selectedId, test: 'testValue'}]);

    this.router.navigate(['../', {id: selectedId}], {relativeTo: this.route});
  }

  showOverview() {
    this.router.navigate(['overview'], {relativeTo: this.route});
  }

  showContact() {
    this.router.navigate(['contact'], {relativeTo: this.route});
  }
}
