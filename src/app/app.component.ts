import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {User} from './user';
import * as D3 from 'd3/index';
import {TestComponent} from './test/test.component';
import {InteractionService} from './interaction.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'Codevolution';
  imgUrl = 'https://i.picsum.photos/id/912/200/200.jpg';
  public name = 'Vishwas';
  public message = '';
  topics = ['Angular', 'React', 'Vue'];
  userModel = new User('Rob', 'rob@test.com', 5556665566, '', 'morning', true);
  userLoggedIn = true;
  @ViewChild(TestComponent) childComponentRef: TestComponent;

  host;
  svg;
  width;
  height;
  private dateUrl = '/assets/countryData/country.csv';

  svgScatter;
  widthScatter;
  heightScatter;

  svgCarScatter;
  widthCarScatter;
  heightCarScatter;
  constructor(private element: ElementRef, private interactionService: InteractionService) {
    this.host = D3.select(this.element.nativeElement);
  }

  ngOnInit(): void {
    this.host.html('');
    // this.buildSVG();
    // this.createBarChart();
    // this.buildScatterSVG();
    // this.createScatterPlot();
    this.buildCarScatterSVG();
    this.createCarScatterPlot();
    console.log('App Component - On Init');
  }

  buildSVG(): void {
    // this.host.html('');
    this.svg = this.host.append('svg')
      .attr('width', '800')
      .attr('height', '800')
      .style('background-color', 'white');
    this.width = +this.svg.attr('width');
    this.height = +this.svg.attr('height');
  }

  buildScatterSVG(): void {
    this.svgScatter = this.host.append('svg')
      .attr('width', '800')
      .attr('height', '500')
      .style('background-color', 'white');
    this.widthScatter = +this.svgScatter.attr('width');
    this.heightScatter = +this.svgScatter.attr('height');
  }

  buildCarScatterSVG(): void {
    this.svgCarScatter = this.host.append('svg')
      .attr('width', '800')
      .attr('height', '500')
      .style('background-color', 'white');
    this.widthCarScatter = +this.svgCarScatter.attr('width');
    this.heightCarScatter = +this.svgCarScatter.attr('height');
  }

  createBarChart() {
    // handle csv data
    D3.csv(this.dateUrl).then(data => {
      data.forEach(d => {
        // @ts-ignore
        d.population = +d.population * 1000;
      });
      console.log(data);
      this.renderData(data);
    });
  }

  renderData(data: D3.DSVRowArray): void {
    const xValue = d => +d.population;
    const yValue = d => d.country;
    const margin = {top: 50, right: 40, bottom: 20, left: 100};
    const innerWidth = this.width - margin.left - margin.right;
    const innerHeight = this.height - margin.top - margin.bottom;

    // xScale is a function, called as xScale(x)
    const xScale = D3.scaleLinear()
      .domain([0, D3.max(data, xValue)])
      .range([0, innerWidth]);

    /**
     * band scale, used for ordinal attributes. see doc here:
     * https://docs.google.com/document/d/1cREDyB_5hXY19da3NjMgm3PJA-pC30dNlz4x-Jv2KHM/edit#
     *
     * Use a data map to compute a function over all these elements of the
     * data array and this function can take as input d (which is just one row) and return d.country.
     * Range will let data elements to be arranged from the top to the bottom
     */

      // first version code

      // const yScale = D3.scaleBand()
      //   .domain(data.map(d => d.country))
      //   .range([0, this.height]);

      // code refactored after remove duplicate code like d.country
    const yScale = D3.scaleBand()
        .domain(data.map(yValue))
        .range([0, innerHeight])
        .padding(0.2); // padding is used to add separation between the bars

    // console.log(yScale.domain());

    /**
     * pass in the y scale so that the axis knows what scale they should use
     */
    const yAxis = D3.axisLeft(yScale);
    const xAxis = D3.axisBottom(xScale);

    /**
     * transform attribute: translate by something in the x-direction and something else
     * in the y direction. And in the x direction meaning pushing it to the right
     */
    const group = this.svg.append('g')
      .attr('transform', `translate(${margin.left}, ${margin.top})`);

    /**
     * two ways:
     * first way: calling a function like this and passing in a selection is quite common in d3
     *   and it is a shorthand for the second way.
     * second way: what you pass in to call is a function that will be invoked with this selection
     */

    // yAxis(group.append('g')); // first way
    group.append('g').call(yAxis)
      .attr('font-size', '1.0em'); // second way
    group.append('g').call(xAxis)
      .attr('transform', `translate(0, ${innerHeight})`)
      .attr('font-size', '0.9em');

    /**
     * bandwidth is the computed width of a single bar
     */
    group.selectAll('rect')
      .data(data)
      .enter()
      .append('rect')
      .attr('y', d => yScale(yValue(d)))
      .attr('width', d => xScale(xValue(d)))
      .attr('height', yScale.bandwidth) // bandwidth is the width of each bar
      .style('background-color', 'green')
      .attr('fill', 'steelblue');

    group.append('text')
      .attr('class', 'title')
      .attr('y', -10)
      .attr('x', 100)
      .attr('font-size', '2em')
      .text('Top 10 Most Populous Countries');
  }

  createScatterPlot() {
    D3.csv(this.dateUrl).then(data => {
      data.forEach(d => {
        // @ts-ignore
        d.population = +d.population;
      });
      console.log(data);
      this.renderScatterData(data);
    });
  }

  renderScatterData(data: D3.DSVRowArray): void {
    const xValue = d => +d.population;
    const yValue = d => d.country;
    const margin = {top: 60, right: 40, bottom: 88, left: 150};
    const innerWidth = this.widthScatter - margin.left - margin.right;
    const innerHeight = this.heightScatter - margin.top - margin.bottom;

    const xScale = D3.scaleLinear()
      .domain([0, D3.max(data, xValue)])
      .range([0, innerWidth])
      .nice(); // used to beautify x axis

    const yScale = D3.scalePoint()
        .domain(data.map(yValue))
        .range([0, innerHeight])
        .padding(0.7); // padding is used to add separation between the bars

    const yAxis = D3.axisLeft(yScale)
      .tickSize(-innerWidth);

    const xAxis = D3.axisBottom(xScale)
      .tickSize(-innerHeight)
      .tickPadding(15);

    const group = this.svgScatter.append('g')
      .attr('transform', `translate(${margin.left}, ${margin.top})`);

    group.append('g').call(yAxis)
      .attr('font-size', '1.0em'); // second way

    group.append('g').call(xAxis)
      .attr('transform', `translate(0, ${innerHeight})`)
      .attr('font-size', '0.9em');

    group.selectAll('circle')
      .data(data)
      .enter()
      .append('circle')
      .attr('cy', d => yScale(yValue(d)))
      .attr('cx', d => xScale(xValue(d)))
      .attr('r', 14) // bandwidth is the width of each bar
      .style('background-color', 'green')
      .attr('fill', 'steelblue');

    group.append('text')
      .attr('class', 'title')
      .attr('y', -10)
      .attr('x', 100)
      .attr('font-size', '2em')
      .text('Top 10 Most Populous Countries');
  }

  createCarScatterPlot() {
    D3.csv('https://vizhub.com/curran/datasets/auto-mpg.csv').then(data => {
      data.forEach(d => {
        // @ts-ignore
        d.mpg = +d.mpg;
        // @ts-ignore
        d.cylinders = +d.cylinders;
        // @ts-ignore
        d.displacement = +d.displacement;
        // @ts-ignore
        d.horsepower = +d.horsepower;
        // @ts-ignore
        d.weight = +d.weight;
        // @ts-ignore
        d.acceleration = +d.acceleration;
        // @ts-ignore
        d.year = +d.year;
      });
      console.log(data);
      this.renderCarScatterData(data);
    });
  }

  renderCarScatterData(data: D3.DSVRowArray): void {
    const title = 'Cars: Horsepower vs. Weight';

    const xValue = d => +d.mpg;
    const xAxisLabel = 'Horsepower';

    const yValue = d => +d.horsepower;
    const yAxisLabel = 'Weight';
    const circleRadius = 10;

    const margin = {top: 80, right: 40, bottom: 88, left: 150};
    const innerWidth = this.widthCarScatter - margin.left - margin.right;
    const innerHeight = this.heightCarScatter - margin.top - margin.bottom;

    const xScale = D3.scaleLinear()
      .domain(D3.extent(data, xValue))
      .range([0, innerWidth])
      .nice(); // used to beautify x axis

    const yScale = D3.scaleLinear()
      .domain(D3.extent(data, yValue))
      .range([0, innerHeight])
      .nice();

    const yAxis = D3.axisLeft(yScale)
      .tickSize(-innerWidth)
      .tickPadding(10);

    // tickpadding is used to push numbers away from the axis a little bit
    const xAxis = D3.axisBottom(xScale)
      .tickSize(-innerHeight)
      .tickPadding(15);

    const group = this.svgCarScatter.append('g')
      .attr('transform', `translate(${margin.left}, ${margin.top})`);

    const yAxisG = group.append('g').call(yAxis)
      .attr('font-size', '1.0em'); // second way

    // removes the domain line.
    yAxisG.selectAll('.domain').remove();

    /**
     * rotate transformation
     * text-anchor: align the text
     */

    yAxisG.append('text')
      .attr('class', 'axis-label')
      .attr('y', -60)
      .attr('x', -innerHeight / 2)
      .attr('fill', 'black')
      .attr('transform', `rotate(-90)`)
      .attr('text-anchor', 'middle')
      .text(yAxisLabel)
      .attr('font-size', '1.6em');

    const xAxisG = group.append('g').call(xAxis)
      .attr('transform', `translate(0, ${innerHeight})`)
      .attr('font-size', '0.9em');

    xAxisG.select('.domain').remove();

    xAxisG.append('text')
      .attr('class', 'axis-label')
      .attr('y', 75)
      .attr('x', innerWidth / 2)
      .attr('fill', 'black')
      .text(xAxisLabel)
      .attr('font-size', '1.6em');

    group.selectAll('circle')
      .data(data)
      .enter()
      .append('circle')
      .attr('cy', d => yScale(yValue(d)))
      .attr('cx', d => xScale(xValue(d)))
      .attr('r', circleRadius) // bandwidth is the width of each bar
      .style('background-color', 'green')
      .attr('fill', 'red')
      .attr('opacity', '0.3');

    group.append('text')
      .attr('class', 'title')
      .attr('y', -20)
      .attr('x', 120)
      .attr('font-size', '2em')
      .text(title);
  }

  /**
   * Access a child components properties and methods
   * in a parent class using the viewChild decorator:
   *
   * if we don't use setTime out there will be error: ExpressionChangedAfterItHasBeenCheckedError
   * see explanation and solution here: https://blog.angular-university.io/angular-debugging/
   */
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.childComponentRef.message = 'Message from parent component';
    });
  }

  greetStudent() {
    this.interactionService.sendMessage('Good Morning');
  }

  appreciateStudent() {
    this.interactionService.sendMessage('Well Done');
  }
}
