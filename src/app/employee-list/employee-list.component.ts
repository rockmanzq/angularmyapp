import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../employee.service';

@Component({
  selector: 'app-employee-list',
  template: `
    <h2>Employee List</h2>
    <h3>{{errorMsg}}</h3>
<!--    <h3>{{errorMsg}}</h3>-->
    <ul *ngFor="let employee of employees">
      <li>{{employee.name}}</li>
    </ul>
  `,
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  public employees = [];
  public errorMsg;
  // tslint:disable-next-line:variable-name
  constructor(private _employeeService: EmployeeService) { }

  /**
   *   the left hand side is basically the argument to the function,
   *   the right hand is the body of the function, we are assigning
   *   the employee data to the employees property
   *
   *   getEmployees() return observable, when we subscribe to the observable
   *   the employee data arrives async, we assign that data to our property using "=>"
   */

  ngOnInit(): void {
    // this.employees = this._employeeService.getEmployees();
    this._employeeService.getEmployees().subscribe(
      (data) => {
        this.employees = data;
      },
      (error) => {
        this.errorMsg = error;
      });
  }
}
